use std::str::FromStr;
use crate::date_error::{DateError, DateErrorKind};
use core::fmt;
use crate::constants::{SECONDS_IN_HOUR, SECONDS_IN_MINUTE, MINUTES_IN_HOUR, MICROSECONDS_IN_SECOND};
use std::cmp::{Ordering, Ord};
use std::time::Duration;

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct Time {
    pub hour:u64,
    pub minute:u64,
    pub second:u64,
    pub microsecond:u64,
}

impl Time {
    pub fn new(hour: u64, minute: u64, second: u64) -> Self {
        Self::new_with_microseconds(hour, minute, second, 0)
    }

    pub fn new_with_microseconds(hour: u64, minute: u64, second: u64, microseconds: u64) -> Self {
        Time { hour, minute, second, microsecond: microseconds }
    }

    pub fn valid(&self) -> bool {
        self.hour < 24 && self.minute < 60 && self.second < 60
    }

    pub fn to_hh_mm_string(&self) -> String {
        format!("{:02}:{:02}", self.hour, self.minute)
    }

    pub fn from_ms_dos_time(mut ms_dos_time:u16) -> Self {
        let second = (ms_dos_time & 0x1f) << 1;
        ms_dos_time >>= 5;
        let minute = ms_dos_time & 0x3f;
        ms_dos_time >>= 6;
        Time::new(ms_dos_time as u64, minute as u64, second as u64)
    }

    pub fn from_seconds(mut seconds:u64) -> Self {
        let hour = seconds / SECONDS_IN_HOUR;
        seconds %= SECONDS_IN_HOUR;
        let minute = seconds / SECONDS_IN_MINUTE;
        seconds %= SECONDS_IN_MINUTE;
        Time::new(hour, minute, seconds)
    }

    pub fn to_seconds(&self) -> u64 {
        self.to_minutes() * SECONDS_IN_MINUTE + self.second
    }

    pub fn to_minutes(&self) -> u64 {
        self.hour * MINUTES_IN_HOUR + self.minute
    }

    pub fn to_microseconds(&self) -> u64 {
        self.to_seconds() * MICROSECONDS_IN_SECOND + self.microsecond
    }

    pub fn to_milliseconds(&self) -> u64 {
        self.to_microseconds() / 1000
    }

    pub fn from_minutes(mut minutes:u64) -> Self {
        let hour = minutes / MINUTES_IN_HOUR;
        minutes %= MINUTES_IN_HOUR;
        Time::new(hour, minutes, 0)
    }

    pub fn from_hours(hour:u64) -> Self {
        Time::new(hour, 0, 0)
    }

    pub fn from_duration(duration:Duration) -> Time {
        Self::from_seconds(duration.as_secs())
    }

    pub fn to_duration(&self) -> Duration {
        Duration::from_secs(self.to_seconds())
    }

    pub fn normalize(&self) -> Time {
        let mut second = self.microsecond / MICROSECONDS_IN_SECOND + self.second;
        let microseconds = self.microsecond % MICROSECONDS_IN_SECOND;
        let mut minute = second / SECONDS_IN_MINUTE + self.minute;
        second = second % SECONDS_IN_MINUTE;
        let hour = minute / MINUTES_IN_HOUR + self.hour;
        minute = minute % MINUTES_IN_HOUR;
        Self::new_with_microseconds(hour, minute, second, microseconds)
    }
}

impl FromStr for Time {
    type Err = DateError;

    fn from_str(date_str: &str) -> Result<Self, Self::Err> {
        let mut split = date_str.split(":");
        let err = DateErrorKind::WrongTimeStringFormat;
        let hour = u64::from_str(split.next().ok_or(err)?).map_err(|_|err)?;
        let minute = u64::from_str(split.next().ok_or(err)?).map_err(|_|err)?;
        let rest = split.next().ok_or(err)?;
        let mut split = rest.split(".");
        let second = u64::from_str(split.next().ok_or(err)?).map_err(|_|err)?;
        let microseconds = match split.next() {
            Some(mut str) => {
                if str.len() > 6 {
                    str = &str[..6];
                }
                u64::from_str(str).map_err(|_|err)? * 10u64.pow(6 - str.len() as u32)
            },
            None => 0,
        };
        Ok(Time::new_with_microseconds(hour, minute, second, microseconds))
    }
}

impl fmt::Display for Time {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:02}:{:02}:{:02}", self.hour, self.minute, self.second)?;
        if self.microsecond > 0 {
            let mut divider = 1;
            while self.microsecond % (divider * 10) == 0 { divider *= 10; }
            let zeros = divider.ilog10();
            let zeros = 6 - zeros as usize;
            write!(f, ".{:0zeros$}", self.microsecond / divider)?;
        }
        Ok(())
    }
}

impl fmt::Debug for Time {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

impl Ord for Time {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.hour != other.hour {
            return self.hour.cmp(&other.hour);
        }
        if self.minute != other.minute {
            return self.minute.cmp(&other.minute);
        }
        if self.second != other.second {
            return self.second.cmp(&other.second);
        }
        self.microsecond.cmp(&other.microsecond)
    }
}

impl PartialOrd for Time {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl std::ops::Sub for Time {
    type Output = Time;

    fn sub(self, rhs: Self) -> Self::Output {
        let mut second = self.to_seconds() - rhs.to_seconds();
        let mut microseconds = self.microsecond;
        if self.microsecond < rhs.microsecond {
            second -= 1;
            microseconds += MICROSECONDS_IN_SECOND;
        }
        microseconds -= rhs.microsecond;
        let mut result = Self::from_seconds(second);
        result.microsecond = microseconds;
        result
    }
}

impl std::ops::Add for Time {
    type Output = Time;

    fn add(self, rhs: Self) -> Self::Output {
        Time::new_with_microseconds(self.hour + rhs.hour, self.minute + rhs.minute,
            self.second + rhs.second, self.microsecond + rhs.microsecond).normalize()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ms_dos_time(){
        assert_eq!(Time::from_ms_dos_time(0x7d1c), Time::new(15, 40, 56));
    }

    #[test]
    fn test_time_cmp() {
        assert!(Time::new(2, 12,31) < Time::new(3, 1,1));
        assert!(Time::new(2, 2,1) > Time::new(2, 1,31));
        assert!(Time::new(2, 3,31) > Time::new(2, 3,30));
        assert_eq!(Time::new(2, 1,1), Time::new(2, 1,1));
        assert!(Time::new_with_microseconds(2, 3,31,11) > Time::new_with_microseconds(2, 3,31, 10))
    }

    #[test]
    fn test_from_str() -> Result<(), DateError>{
        assert_eq!(Time::from_str("21:10:05")?,  Time::new_with_microseconds(21, 10, 5, 0));
        assert_eq!(Time::from_str("21:10:05.779325")?,  Time::new_with_microseconds(21, 10, 5, 779325));
        assert_eq!(Time::from_str("21:10:05.77932599")?,  Time::new_with_microseconds(21, 10, 5, 779325));
        assert_eq!(Time::from_str("21:10:05.77932500")?,  Time::new_with_microseconds(21, 10, 5, 779325));
        assert_eq!(Time::from_str("21:10:05.779000")?,  Time::new_with_microseconds(21, 10, 5, 779000));
        assert_eq!(Time::from_str("21:10:05.779")?,  Time::new_with_microseconds(21, 10, 5, 779000));
        assert_eq!(Time::from_str("21:10:05.034104")?,  Time::new_with_microseconds(21, 10, 5, 034104));
        Ok(())
    }

    #[test]
    fn test_to_string(){
        assert_eq!(Time::new_with_microseconds(21, 10, 5, 0).to_string(), "21:10:05");
        assert_eq!(Time::new_with_microseconds(21, 10, 5, 779325).to_string(), "21:10:05.779325");
        assert_eq!(Time::new_with_microseconds(21, 10, 5, 779000).to_string(), "21:10:05.779");
        assert_eq!(Time::new_with_microseconds(21, 10, 5, 779).to_string(), "21:10:05.000779");
        assert_eq!(Time::new_with_microseconds(21, 10, 5, 34104).to_string(), "21:10:05.034104");
    }

    #[test]
    fn test_time_to_minutes() {
        assert_eq!(Time::new_with_microseconds(2, 3,31,11).to_minutes(), 123);
    }

    #[test]
    fn test_time_to_seconds() {
        assert_eq!(Time::new_with_microseconds(2, 3,31,11).to_seconds(), 7411);
    }

    #[test]
    fn test_time_to_microseconds() {
        assert_eq!(Time::new_with_microseconds(2, 3,31,11).to_microseconds(), 7411000011);
        assert_eq!(Time::new_with_microseconds(2, 3,31,23560).to_microseconds(), 7411023560);
    }

    #[test]
    fn test_time_to_milliseconds() {
        assert_eq!(Time::new_with_microseconds(2, 3,31,23560).to_milliseconds(), 7411023);
    }

    #[test]
    fn test_time_normalize() {
        assert_eq!(Time::new_with_microseconds(42, 81,74,76543213).normalize(), Time::new_with_microseconds(43, 23,30, 543213));
    }

    #[test]
    fn test_time_add() {
        assert_eq!(Time::new_with_microseconds(100, 59,59,999999)
            + Time::new_with_microseconds(0, 0,1, 1),
            Time::new(101,0,1)
        );
    }

    #[test]
    fn test_time_sub() {
        assert_eq!(Time::new_with_microseconds(100, 0,0,0)
                       - Time::new_with_microseconds(0, 0,0, 1),
                   Time::new_with_microseconds(99,59,59, 999999)
        );
    }
}