use crate::date::Date;
use crate::date_error::DateErrorKind;
use crate::time::Time;
use std::fmt;
use std::cmp::Ordering;
use crate::constants::{SECONDS_IN_MINUTE, SECONDS_IN_DAY, MINUTES_IN_HOUR, HOURS_IN_DAY};
use std::ops::Neg;
use std::str::FromStr;
use crate::date_error::DateError;
use crate::utils::parse_util;
use crate::utils::crossplatform_util;

#[derive(Copy, Clone)]
pub struct DateTime {
    pub date:Date,
    pub time:Time,
    pub shift_minutes:isize,
}

impl DateTime {
    pub fn new(date:Date, time:Time, shift_minutes:isize) -> Self {
        DateTime {date, time, shift_minutes}
    }

    pub fn to_iso_8061(&self) -> String{
        format!("{}T{}{}", self.date, self.time, self.shift_string())
    }

    pub fn shift_string(&self) -> String {
        if self.shift_minutes == 0 {return "Z".to_string();}
        if self.shift_minutes.is_positive() {
            format!("+{}", Time::new(0, self.shift_minutes as u64, 0).to_hh_mm_string())
        } else {
            format!("-{}", Time::from_minutes(self.shift_minutes.neg() as u64).to_hh_mm_string())
        }
    }

    pub fn from_seconds_since_unix_epoch(seconds:u64) -> Self {
        let (date, seconds) = Date::from_seconds_since_unix_epoch(seconds);
        let time = Time::from_seconds(seconds);
        DateTime::new(date, time, 0)
    }

    pub fn to_seconds_from_unix_epoch(&self) -> u64 {
        self.date.to_seconds_from_unix_epoch(false) + self.time.to_seconds()
    }

    pub fn to_seconds_from_unix_epoch_gmt(&self) -> u64 {
        (self.to_seconds_from_unix_epoch() as i128 - self.shift_minutes as i128 * SECONDS_IN_MINUTE as i128) as u64
    }

    pub fn now() -> Self {
        Self::from_seconds_since_unix_epoch(crossplatform_util::now_seconds())
    }

    pub fn set_shift(&mut self, minutes:isize){
        if minutes > self.shift_minutes {
            *self = self.add_seconds((minutes - self.shift_minutes) as u64 * SECONDS_IN_MINUTE)
        }
        else {
            *self = self.sub_seconds((self.shift_minutes - minutes) as u64 * SECONDS_IN_MINUTE)
        }
        self.shift_minutes = minutes;
    }

    pub fn add_seconds(&self, seconds:u64) -> Self{
        let total_seconds = self.time.to_seconds() + seconds;
        Self::new(self.date.add_days(total_seconds / SECONDS_IN_DAY),
        Time::from_seconds(total_seconds % SECONDS_IN_DAY), self.shift_minutes)
    }

    pub fn add_time(&self, time:Time) -> Self{
        Self::new(self.date, self.time + time, self.shift_minutes).normalize()
    }

    pub fn sub_seconds(&mut self, seconds:u64) -> Self{
        let mut days = seconds / SECONDS_IN_DAY;
        let seconds = seconds % SECONDS_IN_DAY;
        let time_seconds = self.time.to_seconds();
        let seconds = if time_seconds < seconds {
            days += 1;
            SECONDS_IN_DAY - seconds + time_seconds
        }
        else {
            time_seconds - seconds
        };
        Self::new(self.date.sub_days(days), Time::from_seconds(seconds), self.shift_minutes)
    }

    fn shift_from_str(shift_str: &str) -> Result<isize, DateError> {
        if shift_str.len() == 0 || &shift_str[0..1] == "Z" {
            return Ok(0);
        }

        let mut split = (&shift_str[1..]).split(":");
        let err = || DateErrorKind::WrongTimeShiftStringFormat;
        let hour:u64 = (split.next().ok_or(err())?).parse().or(Err(err()))?;
        let minute:u64 = (split.next().ok_or(err())?).parse().or(Err(err()))?;
        let mut minutes:isize = (hour * MINUTES_IN_HOUR + minute) as isize;
        if &shift_str[0..1] == "-" {
            minutes = 0 - minutes;
        }
        Ok(minutes)
    }

    pub fn normalize(&self) -> DateTime {
        let date = self.date.normalize();
        let mut time = self.time.normalize();
        let days = time.hour / 24;
        time.hour %= 24;
        Self::new(date.add_days(days), time, self.shift_minutes)
    }
}

impl fmt::Display for DateTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.date, self.time)
    }
}

impl fmt::Debug for DateTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

impl Ord for DateTime {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.date.cmp(&other.date) {
            Ordering::Equal if self.shift_minutes == other.shift_minutes => self.time.cmp(&other.time),
            Ordering::Equal => {
                let lhs = self.time.to_minutes() as i64 - self.shift_minutes as i64;
                let rhs = other.time.to_minutes() as i64 - other.shift_minutes as i64;
                match lhs.cmp(&rhs) {
                    Ordering::Equal => (self.time.second + self.time.microsecond).cmp(&(other.time.second + other.time.microsecond)),
                    ordering => ordering,
                }
            },
            ordering => ordering,
        }
    }
}

impl FromStr for DateTime {
    type Err = DateError;

    fn from_str(date_time_str: &str) -> Result<Self, Self::Err> {

        let mut split = date_time_str.split("T");
        let err = || DateErrorKind::WrongDateTimeStringFormat;
        let date:Date = (split.next().ok_or(err())?).parse()?;
        let rest:&str = split.next().ok_or(err())?;
        let (_, index) = parse_util::find_any(rest.as_bytes(), "Z+-".as_bytes()).ok_or(err())?;
        let time:Time = (&rest[..index]).parse()?;
        let shift = DateTime::shift_from_str(&rest[index..])?;
        Ok(DateTime::new(date, time, shift))
    }
}

impl PartialOrd for DateTime {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for DateTime {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl Eq for DateTime {}


impl std::ops::Sub for DateTime {
    type Output = Time;

    fn sub(self, rhs: Self) -> Self::Output {
        self.time + Time::from_hours((self.date - rhs.date) * HOURS_IN_DAY) - rhs.time
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::constants::MINUTES_IN_HOUR;

    #[test]
    fn test_from_seconds_since_unix_epoch(){
        let date_time = DateTime::new(Date::new(2021, 4, 13),
                  Time::new(20, 55, 50), 0);
        assert_eq!(DateTime::from_seconds_since_unix_epoch(1618347350), date_time);
        assert_eq!(date_time.to_seconds_from_unix_epoch(), 1618347350);
    }

    #[test]
    fn test_date_time_cmp() {
        let mut lhs = DateTime::new(Date::new(2019, 12,31), Time::new(12, 0, 0), 0);
        let mut rhs = lhs;
        assert_eq!(lhs, rhs);
        rhs.time.hour += 1;
        assert!(lhs < rhs);
        lhs.shift_minutes = -60;
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_date_time_to_string() {
        let date_time = DateTime::new(Date::new(2021, 7,28),
                                    Time::new(10, 0, 0), -4 * MINUTES_IN_HOUR as isize);
        assert_eq!(date_time.to_iso_8061(), "2021-07-28T10:00:00-04:00");
        assert_eq!(date_time.to_string(), "2021-07-28 10:00:00");
    }

    #[test]
    fn test_shift_from_str() -> Result<(), DateError>{
        assert_eq!(DateTime::shift_from_str("+4:30")?, 270);
        assert_eq!(DateTime::shift_from_str("-4:30")?, -270);
        assert_eq!(DateTime::shift_from_str("Z")?, 0);
        Ok(())
    }

    #[test]
    fn test_date_time_from_str() -> Result<(), DateError>{
        assert_eq!("2021-07-28T10:00:00-4:00".parse::<DateTime>()?,
                   DateTime::new(Date::new(2021, 7,28),
                          Time::new(10, 0, 0), -4 * MINUTES_IN_HOUR as isize));

        assert_eq!("2021-07-28T10:00:00+02:00".parse::<DateTime>()?,
                   DateTime::new(Date::new(2021, 7,28),
                            Time::new(10, 0, 0), 2 * MINUTES_IN_HOUR as isize));

        assert_eq!("2021-07-28T10:00:00Z".parse::<DateTime>()?,
                   DateTime::new(Date::new(2021, 7,28),
                              Time::new(10, 0, 0), 0));
        assert_eq!("2020-01-09T21:10:05.779325Z".parse::<DateTime>()?,
                   DateTime::new(Date::new(2020, 1,9),
                                 Time::new_with_microseconds(21, 10, 5, 779325), 0));

        Ok(())
    }

    #[test]
    fn test_to_seconds_since_unix_epoch_gmt(){
        let date_time = DateTime::new(Date::new(2023, 1, 13),
              Time::new(8, 40, 42), -5 * MINUTES_IN_HOUR as isize);
        assert_eq!(1673617242, date_time.to_seconds_from_unix_epoch_gmt());
        let date_time = DateTime::new(Date::new(2023, 1, 13),
          Time::new(14, 40, 42), 1 * MINUTES_IN_HOUR as isize);
        assert_eq!(1673617242, date_time.to_seconds_from_unix_epoch_gmt());
    }

    #[test]
    fn test_date_time_normalize(){
        let date_time = DateTime::new(Date::new(2023, 1, 13),
                                      Time::new(24, 0, 42), -5 * MINUTES_IN_HOUR as isize);
        let date_time2 = DateTime::new(Date::new(2023, 1, 14),
                                      Time::new(0, 0, 42), -5 * MINUTES_IN_HOUR as isize);
        assert_eq!(date_time.normalize(), date_time2);
    }
}