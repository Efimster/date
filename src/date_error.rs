use std::{error::Error, fmt};

#[derive(Debug, Copy, Clone)]
pub enum DateErrorKind {
    WrongDateStringFormat,
    WrongTimeStringFormat,
    WrongDateTimeStringFormat,
    WrongTimeShiftStringFormat,
    Inner,
}

#[derive(Debug)]
pub struct DateError {
    pub source:Option<Box<dyn Error>>,
    pub kind:DateErrorKind,
}

impl DateError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        DateError {
            source: Some(source),
            kind: DateErrorKind::Inner
        }
    }

    pub fn new_kind(kind:DateErrorKind)->Self{
        DateError {
            source: None,
            kind
        }
    }
}

impl Error for DateError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl fmt::Display for DateErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DateErrorKind::WrongDateStringFormat => write!(f, "wrong date format"),
            DateErrorKind::WrongTimeStringFormat =>  write!(f, "wrong time format"),
            DateErrorKind::WrongDateTimeStringFormat =>  write!(f, "wrong datetime format"),
            DateErrorKind::WrongTimeShiftStringFormat =>  write!(f, "wrong time shift format"),
            DateErrorKind::Inner =>  write!(f, "error in DateError::source"),
        }
    }
}

impl fmt::Display for DateError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<DateErrorKind> for DateError {
    fn from(kind: DateErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for DateError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl DateErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:DateError = self.into();
        error.into()
    }
}
