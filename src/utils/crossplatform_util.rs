#[cfg(not(target_family="wasm"))]
use std::time::SystemTime;
#[cfg(target_family="wasm")]
use wasm_bindgen::prelude::*;

pub fn now_seconds() -> u64 {
    (now_milliseconds() / 1000) as u64
}

#[cfg(not(target_family="wasm"))]
pub fn now_milliseconds() -> u128 {
    SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_millis()
}

#[cfg(target_family="wasm")]
#[wasm_bindgen] extern "C" { #[wasm_bindgen(js_namespace = Date, js_name = now)] fn wasm_date_now() -> f64; }

#[cfg(target_family="wasm")]
pub fn now_milliseconds() -> u128 {
    wasm_date_now() as u128
}