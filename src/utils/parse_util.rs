pub fn find_any<'a, 'b,  T:PartialEq>(input:&'a [T], check_set:&'b [T]) -> Option<(&'a T, usize)> {
    for i in  0 .. input.len(){
        if value_in(&input[i], check_set) {
            return Some((&input[i], i))
        }
    }
    None
}
#[inline]
pub fn value_in<T:PartialEq>(value:&T, set:&[T]) -> bool {
    for v in set {
        if v == value {
            return true;
        }
    }
    false
}