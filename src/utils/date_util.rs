use crate::constants::MONTH_DAYS;

#[inline]
pub fn next_leap_year(year:u64) -> u64 {
    recent_leap_year(year + 4)
}
#[inline]
pub fn recent_leap_year(year:u64) -> u64 {
    year - (year % 4)
}
#[inline]
pub fn leap_year(year:u64) -> bool {
    year % 4 == 0
}
#[inline]
pub fn month_index(month:u64) -> usize {
    (month - 1) as usize
}

pub fn month_days(month:u64, leap_year:bool) -> u64 {
    if leap_year && month == 2 {return MONTH_DAYS[month_index(month)] + 1;}
    MONTH_DAYS[month_index(month)]
}
#[inline]
pub fn before_leap(month:u64) -> bool {
    month < 3
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_date_leap_year(){
        assert!(leap_year(2020));
        assert!(!leap_year(2018));
        assert!(leap_year(2016));
    }

    #[test]
    fn test_date_recent_leap_year(){
        assert_eq!(recent_leap_year(2020), 2020);
        assert_eq!(recent_leap_year(2019), 2016);
    }

    #[test]
    fn test_date_next_leap_year(){
        assert_eq!(next_leap_year(2020), 2024);
        assert_eq!(next_leap_year(2019), 2020);
    }

}