use std::str::FromStr;
use core::{fmt, cmp};
use crate::date_error::{DateError, DateErrorKind};
use crate::constants::{MONTH_DAYS, SECONDS_IN_DAY, FOUR_YEARS_DAYS, LEAP_YEAR_DAYS, REGULAR_YEAR_DAYS, UNIX_EPOCH_YEAR, MONTHS_IN_YEAR, THREE_REGULAR_YEAR_DAYS};
use crate::utils::{crossplatform_util, date_util};
use std::cmp::Ordering;
use crate::utils::date_util::{leap_year, month_days, month_index};

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct Date {
    pub year:u64,
    pub month:u64,
    pub day:u64,
}

impl Date {
    pub fn new(year:u64, month:u64, day:u64) -> Self{
        let date = Date{year, month, day};
        // debug_assert!(date.valid());
        date
    }

    #[inline]
    pub fn leap_year(&self) -> bool {
        date_util::leap_year(self.year)
    }

    #[inline]
    pub fn recent_leap_year(&self) -> u64 {
        date_util::recent_leap_year(self.year)
    }

    #[inline]
    pub fn next_leap_year(&self) -> u64 {
        date_util::next_leap_year(self.year)
    }

    pub fn year_day(&self) -> u64 {
        let mut days:u64 = (0..(self.month_index())).fold(0, |sum, index|sum + MONTH_DAYS[index]);
        if self.month > 2 && self.leap_year() {
            days += 1;
        }
        days + self.day
    }

    pub fn days_to_next_year(&self) -> u64 {
        let total = if self.leap_year() {366u64} else {365};
        total - self.year_day()
    }

    #[inline]
    pub fn month_index(&self) -> usize {
        date_util::month_index(self.month)
    }

    pub fn valid(&self) -> bool {
        if self.month < 1 && self.month > 12 || self.day < 1{
            return false;
        }
        let max_day = MONTH_DAYS[self.month_index()];
        self.day <= max_day || self.month == 2 && self.leap_year() && self.day == 29
    }

    pub fn from_ms_dos_date(mut ms_dos_date:u16) -> Self {
        let day = ms_dos_date & 0x1f;
        ms_dos_date >>= 5;
        let month = ms_dos_date & 0xf;
        ms_dos_date >>= 4;
        Date::new(ms_dos_date as u64 + 1980, month as u64, day as u64)
    }

    pub fn add_days(&self, days:u64) -> Self{
        Self::from_days(self.to_days() + days)
    }

    pub fn from_seconds_since_unix_epoch(seconds:u64) -> (Self, u64) {
        let days = seconds / SECONDS_IN_DAY;
        (Date::new(UNIX_EPOCH_YEAR,1, 1).add_days(days), seconds % SECONDS_IN_DAY)
    }

    pub fn to_seconds_from_unix_epoch(self, included:bool) -> u64 {
        let days = self.to_days() - Date::new(UNIX_EPOCH_YEAR,1, 1).to_days();
        (days + included as u64) * SECONDS_IN_DAY
    }

    pub fn today() -> Self {
        let (date, _) = Self::from_seconds_since_unix_epoch(crossplatform_util::now_seconds());
        date
    }

    pub fn quarter(&self) -> usize {
        (self.month_index() / 3) + 1
    }

    pub fn add_months(&self, months:i64) -> Self {
        let months = (self.year as i64 * 12 + self.month_index() as i64 + months) as u64;
        let month = months % MONTHS_IN_YEAR + 1;
        let year = months / MONTHS_IN_YEAR;
        let day = cmp::min(date_util::month_days(month, date_util::leap_year(year)), self.day);
        Date {
            year,
            month,
            day
        }
    }

    pub fn sub_months(&self, months:i64) -> Self {
        self.add_months(-months)
    }

    pub fn is_month_last_day(&self) -> bool {
        self.day == date_util::month_days(self.month, self.leap_year())
    }

    pub fn month_last_day(&self) -> Self {
        Date {
            year: self.year,
            month: self.month,
            day: date_util::month_days(self.month, self.leap_year())
        }
    }

    pub fn sub_days(&self, days:u64) -> Self {
        Self::from_days(self.to_days() - days)
    }

    /*
        The function does not take "Adoption of the Gregorian calendar https://en.wikipedia.org/wiki/Adoption_of_the_Gregorian_calendar"
        and similar things into consideration.
    */
    pub fn to_days(&self) -> u64 {
        let year_elapsed = self.year - 1;
        let leap_years = year_elapsed / 4;
        let regular_years = year_elapsed - leap_years;
        leap_years * LEAP_YEAR_DAYS + regular_years * REGULAR_YEAR_DAYS  + self.year_day()
    }

    pub fn from_days(days:u64) -> Self {
        let days = days - 1;
        let quarters = days / FOUR_YEARS_DAYS;
        let days  = days % FOUR_YEARS_DAYS;
        let (years, mut days) = if days / THREE_REGULAR_YEAR_DAYS == 0 {
            (days / REGULAR_YEAR_DAYS, days % REGULAR_YEAR_DAYS)
        } else {(3, days % THREE_REGULAR_YEAR_DAYS)};
        let year = (quarters << 2) + years + 1;
        let mut month = 1;
        while days > 0 {
            let month_days = date_util::month_days(month, date_util::leap_year(year));
            if days < month_days {
                break;
            }
            days -= month_days;
            month += 1;
        }
        Date::new(year, month, days + 1)
    }

    /*
        The function does not take "Adoption of the Gregorian calendar https://en.wikipedia.org/wiki/Adoption_of_the_Gregorian_calendar"
        and similar things into consideration.
    */
    pub fn is_monday(&self) -> bool{
        self.to_days() % 7 == 2
    }

    pub fn is_tuesday(&self) -> bool{
        self.to_days() % 7 == 3
    }

    pub fn is_wednesday(&self) -> bool{
        self.to_days() % 7 == 4
    }

    pub fn is_thursday(&self) -> bool{
        self.to_days() % 7 == 5
    }

    pub fn is_friday(&self) -> bool{
        self.to_days() % 7 == 6
    }

    pub fn is_saturday(&self) -> bool{
        self.to_days() % 7 == 0
    }

    pub fn is_sunday(&self) -> bool{
        self.to_days() % 7 == 1
    }

    pub fn is_weekend(&self) -> bool{
        self.to_days() % 7 < 2
    }

    pub fn is_week_day(&self) -> bool{
        self.to_days() % 7 > 1
    }

    pub fn normalize(&self) -> Date {
        let month_index = month_index(self.month) as u64;
        let year = month_index / 12 + self.year;
        let month = month_index as u64 % 12 + 1;
        let mut result = Self::new(year, month, self.day);
        let max_days = month_days(month, leap_year(year));
        if max_days < result.day {
            let add = result.day - max_days;
            result.day = max_days;
            result = result.add_days(add);
        }
        result
    }
}

impl std::ops::Sub for Date {
    type Output = u64;

    fn sub(self, rhs: Self) -> Self::Output {
        self.to_days() - rhs.to_days()
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{:02}-{:02}", self.year, self.month, self.day)
    }
}

impl fmt::Debug for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

impl FromStr for Date {
    type Err = DateError;

    fn from_str(date_str: &str) -> Result<Self, Self::Err> {
        let mut split = date_str.split("-");
        let err = || DateErrorKind::WrongDateStringFormat;
        let year = u64::from_str(split.next().ok_or(err())?).or(Err(err()))?;
        let month = u64::from_str(split.next().ok_or(err())?).or(Err(err()))?;
        let day = u64::from_str(split.next().ok_or(err())?).or(Err(err()))?;
        Ok(Date::new(year, month, day))
    }
}

impl Ord for Date {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.year != other.year {
            return self.year.cmp(&other.year);
        }
        if self.month != other.month {
            return self.month.cmp(&other.month);
        }
        self.day.cmp(&other.day)
    }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_date_year_day(){
        let date = Date::new(2020, 1,1);
        assert_eq!(date.year_day(), 1);
        let date = Date::new(2018, 2,28);
        assert_eq!(date.year_day(), 59);
        let date = Date::new(2016, 12,31);
        assert_eq!(date.year_day(), 366);
    }

    #[test]
    fn test_date_sub(){
        let date_1 = Date::new(2020, 1,1);
        let date_2 = Date::new(2019, 12,31);
        assert_eq!(date_1 - date_2, 1);
        let date_1 = Date::new(2020, 1,1);
        let date_2 = Date::new(2016, 1,1);
        assert_eq!(date_1 - date_2, 1461);
        let date_1 = Date::new(2020, 1,1);
        let date_2 = Date::new(2016, 3,1);
        assert_eq!(date_1 - date_2, 1401);
        let date_1 = Date::new(2020, 3,4);
        let date_2 = Date::new(2016, 3,1);
        assert_eq!(date_1 - date_2, 1464);
        // let date_1 = Date::new(2019, 12,31);
        // let date_2 = Date::new(2020, 1,1);
        // assert_eq!(date_1 - date_2, Date::from_days(1));
        let date_1 = Date::new(2021, 3,2);
        let date_2 = Date::new(2019, 12,31);
        assert_eq!(date_1 - date_2, 427);
        let date_1 = Date::new(2021, 3,2);
        let date_2 = Date::new(2020, 12,31);
        assert_eq!(date_1 - date_2, 61);
        let date_1 = Date::new(2021, 3,2);
        let date_2 = Date::new(2020, 1,15);
        assert_eq!(date_1 - date_2, 412);
        let date_1 = Date::new(2020, 12,31);
        let date_2 = Date::new(2020, 1,1);
        assert_eq!(date_1 - date_2, 365);
    }

    #[test]
    fn test_date_from_str() -> Result<(), DateError>{
        assert_eq!(Date::from_str("2020-02-29")?, Date::new(2020, 2, 29));
        Ok(())
    }

    #[test]
    fn test_add_days(){
        assert_eq!(Date::new(2019, 12,31).add_days(1), Date::new(2020, 1,1));
        assert_eq!(Date::new(2019, 12,31).add_days(3753), Date::new(2030, 4,10));
        assert_eq!(Date::new(2019, 2,28).add_days(365), Date::new(2020, 2,28));
        assert_eq!(Date::new(2019, 2,28).add_days(366), Date::new(2020, 2,29));
        assert_eq!(Date::new(2019, 3,1).add_days(366), Date::new(2020, 3,1));
        assert_eq!(Date::new(2018, 1,1).add_days(1198), Date::new(2021, 4,13));
    }

    #[test]
    fn test_ms_dos_date(){
        assert_eq!(Date::from_ms_dos_date(0x354b), Date::new(2006, 10, 11));
    }

    #[test]
    fn test_date_cmp() {
        assert!(Date::new(2019, 12,31) < Date::new(2020, 1,1));
        assert!(Date::new(2020, 2,1) > Date::new(2020, 1,31));
        assert!(Date::new(2020, 3,31) > Date::new(2020, 3,30));
        assert_eq!(Date::new(2020, 1,1), Date::new(2020, 1,1));
    }

    #[test]
    fn test_add_months() {
        assert_eq!(Date::new(2019, 12,31).add_months(2), Date::new(2020, 2,29));
        assert_eq!(Date::new(2019, 12,31).add_months(26), Date::new(2022, 2,28));
        assert_eq!(Date::new(2019, 12,31).add_months(1), Date::new(2020, 1,31));
        assert_eq!(Date::new(2020, 2,29).add_months(-2), Date::new(2019, 12,29));
    }

    #[test]
    fn test_is_month_last_day() {
        assert!(Date::new(2019, 12,31).is_month_last_day());
        assert!(!Date::new(2019, 12,30).is_month_last_day());
        assert!(Date::new(2019, 2,28).is_month_last_day());
        assert!(!Date::new(2020, 2,28).is_month_last_day());
        assert!(Date::new(2020, 2,29).is_month_last_day());
    }

    #[test]
    fn test_month_last_day() {
        assert_eq!(Date::new(2019, 2,2).month_last_day(), Date::new(2019, 2,28));
        assert_eq!(Date::new(2020, 2,2).month_last_day(), Date::new(2020, 2,29));
    }

    #[test]
    fn test_to_seconds_from_unix_epoch() {
        assert_eq!(Date::new(1970, 1,1).to_seconds_from_unix_epoch(false), 0);
        assert_eq!(Date::new(1970, 1,1).to_seconds_from_unix_epoch(true), SECONDS_IN_DAY);
    }

    #[test]
    fn test_to_days() {
        assert_eq!(Date::new(1, 1,1).to_days(), 1);
        assert_eq!(Date::new(1, 12,31).to_days(), 365);
        assert_eq!(Date::new(4, 2,29).to_days(), 1155);
        assert_eq!(Date::new(5, 1,1).to_days(), 1462);
    }

    #[test]
    fn test_from_days() {
        assert_eq!(Date::from_days(1), Date::new(1, 1,1));
        assert_eq!(Date::from_days(365), Date::new(1, 12,31));
        assert_eq!(Date::from_days(1155), Date::new(4, 2,29));
        assert_eq!(Date::from_days(1462), Date::new(5, 1,1));
    }

    #[test]
    fn test_is_monday() {
        assert_eq!(Date::new(2021, 8,2).is_monday(), true);
        assert_eq!(Date::new(2021, 8,3).is_monday(), false);
    }

    #[test]
    fn test_is_tuesday() {
        assert_eq!(Date::new(2021, 8,3).is_tuesday(), true);
        assert_eq!(Date::new(2021, 8,4).is_tuesday(), false);
    }

    #[test]
    fn test_is_wednesday() {
        assert_eq!(Date::new(2021, 8,4).is_wednesday(), true);
        assert_eq!(Date::new(2021, 8,5).is_wednesday(), false);
    }

    #[test]
    fn test_is_thursday() {
        assert_eq!(Date::new(2021, 8,5).is_thursday(), true);
        assert_eq!(Date::new(2021, 8,6).is_thursday(), false);
    }

    #[test]
    fn test_is_friday() {
        assert_eq!(Date::new(2021, 8,6).is_friday(), true);
        assert_eq!(Date::new(2021, 8,7).is_friday(), false);
    }

    #[test]
    fn test_is_saturday() {
        assert_eq!(Date::new(2021, 8,7).is_saturday(), true);
        assert_eq!(Date::new(2021, 8,8).is_saturday(), false);
    }

    #[test]
    fn test_is_sunday() {
        assert_eq!(Date::new(2021, 8,8).is_sunday(), true);
        assert_eq!(Date::new(2021, 8,9).is_sunday(), false);
    }

    #[test]
    fn test_add_sub_days(){
        let a = Date::new(2020,12,31);
        assert_eq!(a.add_days(1).sub_days(1), a);
    }

    #[test]
    fn test_sub_months() {
        assert_eq!(Date::new(2020, 2,29).sub_months(2), Date::new(2019, 12,29));
        assert_eq!(Date::new(2020, 4,30).sub_months(2), Date::new(2020, 2,29));
        assert_eq!(Date::new(2022, 2,28).sub_months(26), Date::new(2019, 12,28));
        assert_eq!(Date::new(2020, 1,31).sub_months(1), Date::new(2019, 12,31));
    }

    #[test]
    fn test_normalize() {
        assert_eq!(Date::new(2020, 49,32).normalize(), Date::new(2024, 2, 1));
        assert_eq!(Date::new(2020, 49,60).normalize(), Date::new(2024, 2, 29));
        assert_eq!(Date::new(2020, 49,61).normalize(), Date::new(2024, 3, 1));
    }
}
